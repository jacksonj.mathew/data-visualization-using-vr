﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;

public class ControllerMove : MonoBehaviour
{
    Controller controller;
    float HandPalmPitch;
    float HandPalmYaw;
    float HandPalmRoll;
    float HandWristRot;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        controller = new Controller();
        Frame frame = controller.Frame();
        List<Hand> hands = frame.Hands;
        if (frame.Hands.Count == 1)
        {
            Hand firstHand = hands[0];

            HandPalmYaw = hands[0].PalmNormal.Yaw;
            
            if(hands[0].IsLeft)
            {
                if (HandPalmYaw > -2f && HandPalmYaw < 3.5f)
                {
                    //Debug.Log("Move hand start forward");
                    transform.Translate(new Vector3(0, 0, 2 * Time.deltaTime));
                    //Debug.Log("Move hand end forward");
                }
                else if (HandPalmYaw < -2.2f)
                {
                    //Debug.Log("Move hand start backward");
                    transform.Translate(new Vector3(0, 0, -2 * Time.deltaTime));
                    //Debug.Log("Move hand end backward");
                }
            } else
            {
                if (HandPalmYaw > -2f && HandPalmYaw < 3.5f)
                {
                    //Debug.Log("Move hand start forward");
                    transform.Translate(new Vector3(2 * Time.deltaTime, 0, 0));
                    //Debug.Log("Move hand end forward");
                }
                else if (HandPalmYaw < -2.2f)
                {
                    //Debug.Log("Move hand start backward");
                    transform.Translate(new Vector3(-2 * Time.deltaTime, 0, 0));
                    //Debug.Log("Move hand end backward");
                }
            }
             
        } else if (frame.Hands.Count > 1)
        {
            Hand firstHand = hands[0];
            
            HandPalmYaw = hands[0].PalmNormal.Yaw;
            if (HandPalmYaw > -2f && HandPalmYaw < 3.5f)
            {
                //Debug.Log("Move hand start forward");
                transform.Translate(new Vector3(0, 2 * Time.deltaTime, 0));
                //Debug.Log("Move hand end forward");
            }
            else if (HandPalmYaw < -2.2f)
            {
                //Debug.Log("Move hand start backward");
                transform.Translate(new Vector3(0, -2 * Time.deltaTime, 0));
                //Debug.Log("Move hand end backward");
            }

        }
    }
}
