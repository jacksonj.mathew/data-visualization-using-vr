﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using Mono.Data.SqliteClient;
using System;

public class DatapointReader : MonoBehaviour
{
    public static List<Dictionary<string,object>> getDataPoints(string tableName, int limit,string condition)
    {
        var list = new List<Dictionary<string, object>>();

        // The name of the db.
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db";
        //Debug.Log(dbName);
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery;
        if ((tableName == "Banking_Data_TSNE" && condition == "WHERE outcome = 1") || (tableName == "PCA_Data") || (tableName == "Election_Data_TSNE" && condition == "WHERE outcome = 0"))
        {
            sqlQuery = "SELECT * FROM " + tableName + " " + condition + " LIMIT " + limit;
        } else
        {
            int playerId = getPlayerId();
            int offset=1 + (playerId % 5);
            sqlQuery = "SELECT * FROM " + tableName + " " + condition + " LIMIT 50 OFFSET " + (limit*offset);
        }
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while(reader.Read())
        {
            
            int id = reader.GetInt32(0);
            float xValue = reader.GetFloat(1);
            float yValue = reader.GetFloat(2);
            float zValue = reader.GetFloat(3);
            int outcome = reader.GetInt32(4);
            int expectedOutcome;

            var entry = new Dictionary<string, object>(); // Creates dictionary object
            entry["id"] = id;
            entry["X-Value"] = xValue;
            entry["Y-Value"] = yValue;
            entry["Z-Value"] = zValue;
            entry["outcome"] = outcome;
            if ((tableName == "Election_Data_TSNE") || (tableName == "Election_Data_PCA"))
            {
                expectedOutcome = reader.GetInt32(5);
                entry["expected-outcome"] = expectedOutcome;
            }

            list.Add(entry);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return list;

    }


    public static List<Dictionary<string, object>> getQuestionPoints(string tableName, int limit, string condition)
    {
        var list = new List<Dictionary<string, object>>();

        // The name of the db.
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db";
        //Debug.Log(dbName);
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "SELECT * FROM " + tableName + " " + condition + " LIMIT " + limit;
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            int id = reader.GetInt32(0);
            float xValue = reader.GetFloat(1);
            float yValue = reader.GetFloat(2);
            float zValue = reader.GetFloat(3);
            int outcome = reader.GetInt32(4);

            var entry = new Dictionary<string, object>(); // Creates dictionary object
            entry["id"] = id;
            entry["X-Value"] = xValue;
            entry["Y-Value"] = yValue;
            entry["Z-Value"] = zValue;
            entry["outcome"] = outcome;
            list.Add(entry);
        }
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return list;
    }

        public static void createPlayerId(string startTime)
    {
        // Creating Player id
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db"; // The name of the db
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database
        
        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "INSERT INTO user_data ('started_at','completed_at','activity_duration') VALUES (" + startTime + ",'','')"; //creates a new playerid
        //Debug.Log(sqlQuery);
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        string sqlQuery2 = "INSERT INTO user_log ('tutorial_time','q1_time_pca','q2_time_pca','q3_time_pca','q4_time_pca','q5_time_pca','q1_time_tsne','q2_time_tsne','q3_time_tsne','q4_time_tsne','q5_time_tsne') VALUES (0,0,0,0,0,0,0,0,0,0,0);"; //creates a new playerid
        dbcmd.CommandText = sqlQuery2;
        IDataReader reader2 = dbcmd.ExecuteReader();

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }

    public static int getPlayerId()
    {
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db"; // The name of the db
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();

        // Retrieve the latest created player_id
        string retrieveQuery = "SELECT MAX(id)  FROM user_data;";
        dbcmd.CommandText = retrieveQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        int playerId = 0;
        while (reader.Read())
        {
            playerId = reader.GetInt32(0);
        }

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;

        return playerId;
    }

    public static void sceneLog(double duration, string currentScene,int question = 0)
    {
        string updateString;
        if (question==0)
        {
            updateString = " 'tutorial_time' = " + duration + " ";
        } else
        {
            string postfix;
            if(currentScene == "PcaQuestions") { postfix = "pca"; }
            else if(currentScene == "TsneQuestions") { postfix = "tsne"; }
            else if(currentScene == "TsneQuestions2") { postfix = "tsne2"; }
            else { postfix = "pca2"; }
            updateString = " 'q" + question + "_time_" + postfix + "' = " + duration + " ";
        }
        

        int playerId = getPlayerId();

        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db"; // The name of the db
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery = "UPDATE user_log SET" + updateString + "WHERE \"id\" = " + playerId;
        Debug.Log(sqlQuery);
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
}
