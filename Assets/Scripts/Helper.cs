﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper
{
    public static GameObject createPlane(float x1, float y1, float z1, float x2, float y2, float z2)
    {
        GameObject go = new GameObject("Plane");
        MeshFilter mf = go.AddComponent(typeof(MeshFilter)) as MeshFilter;
        MeshRenderer mr = go.AddComponent(typeof(MeshRenderer)) as MeshRenderer;

        Mesh m = new Mesh();
        m.vertices = new Vector3[]
        {
            new Vector3(x1, y1, z1),
            new Vector3(x1,y2,z1),
            new Vector3(x2,y2,z2),
            new Vector3(x2,y1,z2)
        };

        m.uv = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,1),
            new Vector2(1,1),
            new Vector2(1,0)
        };

        m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        mf.mesh = m;
        m.RecalculateBounds();
        m.RecalculateNormals();
        return go;
    }
}
