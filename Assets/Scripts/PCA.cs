﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Statistics.Analysis;
using Mono.Data.SqliteClient;
using System.Data;

public class PCA : MonoBehaviour
{
    public static void Implement(double[][] dataMatrix, int[] outcomeData, int flag, int[] expectedOutcome = null)
    {
        // PCA cordinates calculation
        var pca = new PrincipalComponentAnalysis()
        {
            Method = PrincipalComponentMethod.Center,
            Whiten = true
        };
        MultivariateLinearRegression transform = pca.Learn(dataMatrix);
        pca.NumberOfOutputs = 3;
        double[][] outputPca = pca.Transform(dataMatrix);

        // Writting PCA data to the database
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db"; // The name of the db
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery;
        if (flag==0)
            sqlQuery = "DELETE FROM Banking_data_PCA;"; //Truncate Query
        else
            sqlQuery = "DELETE FROM Election_data_PCA;"; //Truncate Query
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        for (int i = 0; i < outputPca.Length; i++) {
            //Insert Query
            string insertQuery;
            if (flag == 0)
                insertQuery = "INSERT INTO Banking_data_PCA ('x-value','y-value','z-value','outcome') VALUES (" + outputPca[i][0] + "," + outputPca[i][1] + "," + outputPca[i][2] + "," + outcomeData[i] + ")";
            else
                insertQuery = "INSERT INTO Election_data_PCA ('x-value','y-value','z-value','outcome','expected-outcome') VALUES (" + outputPca[i][0] + "," + outputPca[i][1] + "," + outputPca[i][2] + "," + outcomeData[i] + "," + expectedOutcome[i] +")";
            dbcmd.CommandText = insertQuery;
            reader = dbcmd.ExecuteReader();
        }
        //closing datbase connection
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
}
