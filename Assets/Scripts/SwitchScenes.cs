﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SwitchScenes : MonoBehaviour
{
    private bool loadingStarted = false;
    private float secondsLeft = 0;
    private string currentScene;
    private string nextScene;
    private DateTime startTime;
    private int questionCounter = 1;

    // Start is called before the first frame update
    void Start()
    {
        startTime = LoadData.getCurrentTimestamp();
        currentScene = SceneManager.GetActiveScene().name;
        Debug.Log("Current Scene: " + currentScene);
        if(currentScene == "BootScene")
            StartCoroutine(DelayLoadLevel(15));
    }

    // Update is called once per frame
    void Update()
    {
        if(currentScene != "BootScene")
        {
            int playerId = DatapointReader.getPlayerId();
            if(playerId%2 == 0)
            {
                switch (currentScene)
                {
                    case "Initial Scene":
                        nextScene = "PcaQuestions";
                        break;
                    case "PcaQuestions":
                        nextScene = "TsneQuestions";
                        break;
                    case "TsneQuestions":
                        nextScene = "PcaQuestion2";
                        break;
                    case "PcaQuestion2":
                        nextScene = "TsneQuestions2";
                        break;
                    case "TsneQuestions2":
                        nextScene = "ThankyouScene";
                        break;
                }
            } else
            {
                switch (currentScene)
                {
                    case "Initial Scene":
                        nextScene = "TsneQuestions";
                        break;
                    case "TsneQuestions":
                        nextScene = "PcaQuestions";
                        break;
                    case "PcaQuestions":
                        nextScene = "TsneQuestions2";
                        break;
                    case "TsneQuestions2":
                        nextScene = "PcaQuestion2";
                        break;
                    case "PcaQuestion2":
                        nextScene = "ThankyouScene";
                        break;
                }
            }
            
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (currentScene == "ThankyouScene")
                    Application.Quit();
                loadNextScene();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if ((currentScene == "PcaQuestion2" || currentScene == "TsneQuestions2") && questionCounter >= 2)
                    loadNextScene();
                if (questionCounter == 6)
                    loadNextScene();
                else
                    logQuestionTimestamp(questionCounter);
            }
        }
    }

    IEnumerator DelayLoadLevel(float seconds)
    {
        secondsLeft = seconds;
        loadingStarted = true;
        do
        {
            yield return new WaitForSeconds(1);
        } while (--secondsLeft > 0);
        SceneManager.LoadScene("Initial Scene");
    }

    void OnGUI()
    {
        if (loadingStarted)
            GUI.Label(new Rect(0, 0, 100, 20), secondsLeft.ToString());
    }

    void loadNextScene()
    {
        DateTime endTime = LoadData.getCurrentTimestamp();
        double duration = LoadData.getDuration(startTime, endTime);
        if(currentScene == "Initial Scene")
            DatapointReader.sceneLog(duration, currentScene);
        SceneManager.LoadScene(nextScene);
    }

    void logQuestionTimestamp(int counter)
    {
        DateTime endTime = LoadData.getCurrentTimestamp();
        double duration = LoadData.getDuration(startTime, endTime);
        questionCounter++;
        /*if (questionCounter <= 5)
            questionCounter++;
        else
            questionCounter = 5;*/
        startTime = endTime;
        DatapointReader.sceneLog(duration, currentScene,counter);
    }
}
