﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class QuestionMesh : MonoBehaviour
{
    [SerializeField]
    private TextMesh _questionText;

    [SerializeField]
    private TextMesh _questionData;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        var playerId = DatapointReader.getPlayerId().ToString();
        _questionText.text = "";
        _questionText.text += "Player Id: " + playerId;
        _questionText.text += "\nPlease provide the outcomes for the following data point";
    }
}
