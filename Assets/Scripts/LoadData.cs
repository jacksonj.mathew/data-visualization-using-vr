﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using System;

public class LoadData : MonoBehaviour
{
    // List for holding data from CSV reader
    private List<Dictionary<string, object>> pointList;

    // Define delimiters, regular expression craziness
    static string SPLIT_RE = @";(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";

    // Define delimiters, regular expression craziness
    static string SPLIT_RE2 = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";

    // Define line delimiters, regular experession craziness
    static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";

    static char[] TRIM_CHARS = { '\"' };

    // Start is called before the first frame update
    void Start()
    {
        //Creates the starting time stamp
        string startTime = getTimestampString(DateTime.Now);

        //Function to create a new player Id
        DatapointReader.createPlayerId(startTime);

        //Function to get the last player id
        int player_id = DatapointReader.getPlayerId();
        Debug.Log("Player Id: " + player_id);

        // Function to read data from csv and implement PCA and TSNE
        //readCSV();
        //readElectionCSV();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void readCSV()
    {
        string csvFile = "Assets/Datasets/Dataset.csv";
        string data = File.ReadAllText(csvFile);
        var lines = Regex.Split(data, LINE_SPLIT_RE); // Split data.text into lines using LINE_SPLIT_RE characters

        var header = Regex.Split(lines[0], SPLIT_RE); //Split header (element 0)
        string queryHeader = lines[0].Replace(';',',');
        queryHeader = Regex.Replace(queryHeader,"\"", "");

        // Enter Data
        var length = lines.Length;
        double[][] dataMatrix = new double[length-1][];
        int[] outcomeData = new int[length];
        for (int i=1; i<length; i++)
        {
            string insertLine = lines[i].Replace("\"nonexistent\"","0"); // nonexistent coded as 0
            insertLine = insertLine.Replace("\"failure\"", "1"); // failure coded as 1
            insertLine = insertLine.Replace("\"success\"", "2"); // success coded as 2

            insertLine = insertLine.Replace("\"no\"", "0"); // no coded as 0
            insertLine = insertLine.Replace("\"yes\"", "1"); // yes coded as 1
            insertLine = insertLine.Replace("\"unknown\"", "-1"); // unknown coded as 2

            insertLine = insertLine.Replace("\"single\"", "1"); // single coded as 1
            insertLine = insertLine.Replace("\"married\"", "2"); // married coded as 2
            insertLine = insertLine.Replace("\"divorced\"", "3"); // divorced coded as 3

            insertLine = insertLine.Replace("\"telephone\"", "1"); // telephone coded as 1
            insertLine = insertLine.Replace("\"cellular\"", "2"); // cellular coded as 2

            insertLine = insertLine.Replace("\"unemployed\"", "0"); // unemployed coded as 0
            insertLine = insertLine.Replace("\"housemaid\"", "1"); // housemaid coded as 1
            insertLine = insertLine.Replace("\"student\"", "2"); // student coded as 2
            insertLine = insertLine.Replace("\"admin.\"", "3"); // admin. coded as 3
            insertLine = insertLine.Replace("\"blue-collar\"", "4"); // blue-collar coded as 4
            insertLine = insertLine.Replace("\"technician\"", "5"); // technician coded as 5
            insertLine = insertLine.Replace("\"management\"", "6"); // management coded as 6
            insertLine = insertLine.Replace("\"services\"", "7"); // services coded as 7
            insertLine = insertLine.Replace("\"retired\"", "8"); // retired coded as 8
            insertLine = insertLine.Replace("\"entrepreneur\"", "9"); // entrepreneur coded as 9
            insertLine = insertLine.Replace("\"self-employed\"", "10"); // self-employed coded as 10

            insertLine = insertLine.Replace("\"illiterate\"", "0"); // illiterate coded as 0
            insertLine = insertLine.Replace("\"basic.4y\"", "1"); // basic.4y coded as 1
            insertLine = insertLine.Replace("\"basic.6y\"", "2"); // basic.6y coded as 2
            insertLine = insertLine.Replace("\"basic.9y\"", "3"); // basic.9y coded as 3
            insertLine = insertLine.Replace("\"high.school\"", "4"); // high.school coded as 4
            insertLine = insertLine.Replace("\"professional.course\"", "5"); // professional.course coded as 5
            insertLine = insertLine.Replace("\"university.degree\"", "6"); // university.degree coded as 6

            insertLine = insertLine.Replace("\"jan\"", "1"); // jan coded as 1
            insertLine = insertLine.Replace("\"feb\"", "2"); // feb coded as 2
            insertLine = insertLine.Replace("\"mar\"", "3"); // mar coded as 3
            insertLine = insertLine.Replace("\"apr\"", "4"); // apr coded as 4
            insertLine = insertLine.Replace("\"may\"", "5"); // may coded as 5
            insertLine = insertLine.Replace("\"jun\"", "6"); // jun coded as 6
            insertLine = insertLine.Replace("\"jul\"", "7"); // jul coded as 7
            insertLine = insertLine.Replace("\"aug\"", "8"); // aug coded as 8
            insertLine = insertLine.Replace("\"sep\"", "9"); // sep coded as 9
            insertLine = insertLine.Replace("\"oct\"", "10"); // oct coded as 10
            insertLine = insertLine.Replace("\"nov\"", "11"); // nov coded as 11
            insertLine = insertLine.Replace("\"dec\"", "12"); // dec coded as 12

            insertLine = insertLine.Replace("\"mon\"", "1"); // mon coded as 1
            insertLine = insertLine.Replace("\"tue\"", "2"); // tue coded as 2
            insertLine = insertLine.Replace("\"wed\"", "3"); // wed coded as 3
            insertLine = insertLine.Replace("\"thu\"", "4"); // thu coded as 4
            insertLine = insertLine.Replace("\"fri\"", "5"); // fri coded as 5

            var insertLineString = Regex.Split(insertLine, SPLIT_RE);
            double[] insertLineDouble = new double[header.Length-1];

            for (int j=0;j<header.Length-1;j++)
            {
                //dataMatrix[i-1][j] = System.Convert.ToDouble(insertLineMatrix[j]);
                insertLineDouble[j] = System.Convert.ToDouble(insertLineString[j]);
                //Debug.Log(dataMatrix[i - 1][j]);
            }
            dataMatrix[i - 1] = insertLineDouble;
            outcomeData[i - 1] = System.Convert.ToInt32(insertLineString[header.Length - 1]);
        }
        //PCA.Implement(dataMatrix,outcomeData,0);
        //TSNEData.Implement(dataMatrix,outcomeData,header.Length,0);

    }

    public void readElectionCSV()
    {
        string csvFile = "Assets/Datasets/DatasetElection.csv";
        string data = File.ReadAllText(csvFile);
        var lines = Regex.Split(data, LINE_SPLIT_RE); // Split data.text into lines using LINE_SPLIT_RE characters

        var header = Regex.Split(lines[0], SPLIT_RE2); //Split header (element 0)
        string queryHeader = lines[0].Replace(';', ',');
        queryHeader = Regex.Replace(queryHeader, "\"", "");

        // Enter Data
        var length = lines.Length;
        double[][] dataMatrix = new double[length - 1][];
        int[] outcomeData = new int[length];
        int[] expectedOutcomeData = new int[length];
        for (int i = 1; i < length; i++)
        {
            var insertLineString = Regex.Split(lines[i], SPLIT_RE2);
            double[] insertLineDouble = new double[header.Length];
            for (int j = 0; j < header.Length; j++)
            {
                //dataMatrix[i-1][j] = System.Convert.ToDouble(insertLineMatrix[j]);
                insertLineDouble[j] = System.Convert.ToDouble(insertLineString[j]);
                //Debug.Log(dataMatrix[i - 1][j]);
            }
            dataMatrix[i - 1] = insertLineDouble;
            if(insertLineDouble[header.Length-1] + insertLineDouble[header.Length - 3] > 0.5)
                outcomeData[i - 1] = 0;
            else
                outcomeData[i - 1] = 1;
            if (insertLineDouble[header.Length - 3] > 0.5)
                expectedOutcomeData[i - 1] = 0;
            else
                expectedOutcomeData[i - 1] = 1;
        }

        //PCA.Implement(dataMatrix,outcomeData,1,expectedOutcomeData);
        //TSNEData.Implement(dataMatrix,outcomeData,header.Length,1,expectedOutcomeData);
    }

    public static String getTimestampString(DateTime value)
    {
        return value.ToString("yyyyMMddHHmmssffff");
    }

    public static DateTime getCurrentTimestamp()
    {
        return DateTime.Now;
    }

    public static double getDuration(DateTime startTime,DateTime endTime)
    {
        TimeSpan duration = endTime - startTime;
        return duration.TotalSeconds;
    }
}
