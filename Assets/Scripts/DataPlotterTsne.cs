﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class DataPlotterTsne : MonoBehaviour
{
    // List for holding data from the database
    private List<Dictionary<string, object>> pointList;

    // Indices for columns to be assigned
    public int columnX = 1;
    public int columnY = 2;
    public int columnZ = 3;
    public int columnOutcome = 4;

    // Full column names
    public string xName;
    public string yName;
    public string zName;
    public string outcome;

    // The prefab for the data points that will be instantiated
    public GameObject PointPrefab;

    // Object which will contain instantiated prefabs in hiearchy
    public GameObject Plotter;

    // Start is called before the first frame update

    // Start is called before the first frame update
    void Start()
    {
        pointList = DatapointReader.getDataPoints("Banking_Data_TSNE", 50, "WHERE outcome = 0");

        List<Dictionary<string, object>> pointList1;
        pointList1 = DatapointReader.getDataPoints("Banking_Data_TSNE", 50, "WHERE outcome = 1");


        for (int i = 0; i < pointList1.Count; i++)
        {
            pointList.Add(pointList1[i]);
        }

        // Declare list of strings, fill with keys (column names)
        List<string> columnList = new List<string>(pointList[1].Keys);

        // Assign column name from columnList to Name variables
        xName = columnList[columnX];
        yName = columnList[columnY];
        zName = columnList[columnZ];
        outcome = columnList[columnOutcome];
        //Loop through Pointlist
        for (var i = 0; i < pointList.Count; i++)
        {
            // Get value in poinList at ith "row", in "column" Name
            double x = System.Convert.ToSingle(pointList[i][xName]);
            double y = System.Convert.ToSingle(pointList[i][yName]);
            double z = System.Convert.ToSingle(pointList[i][zName]);
            int result = System.Convert.ToInt32(pointList[i][outcome]);

            //Scaling to spread the plot
            x = Math.Round(x, 2);
            y = Math.Round(y, 2);
            z = Math.Round(z, 2);
            //Debug.Log("x = " + x + " \ty = " + y + "\tz = " + z + "\toutcome = " + result);

            //instantiate the prefab with coordinates defined above
            Instantiate(PointPrefab, new Vector3((float)x, (float)y, (float)z), Quaternion.identity);

            // Instantiate as gameobject variable so that it can be manipulated within loop
            GameObject dataPoint = Instantiate(
                    PointPrefab,
                    new Vector3((float)x, (float)y, (float)z),
                    Quaternion.identity);

            // Make child of PointHolder object, to keep points within container in hierarchy
            dataPoint.transform.parent = Plotter.transform;

            // Assigns original values to dataPointName
            string dataPointName =
                pointList[i][xName] + " "
                + pointList[i][yName] + " "
                + pointList[i][zName];

            // Assigns name to the prefab
            dataPoint.transform.name = dataPointName;

            // Gets material color and sets it to a new RGB color we define
            if (result == 0)
            {
                dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            }
            else
            {
                dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
            }
        }
        prepareQuestionPoints();
    }

    public void prepareQuestionPoints()
    {
        List<Dictionary<string, object>> questionList;
        questionList = DatapointReader.getQuestionPoints("Banking_Data_TSNE", 5, "WHERE id IN(150,250,350,450,550)");
        Debug.Log("pointList Count:" + questionList.Count);

        for (var i = 0; i < questionList.Count; i++)
        {
            // Get value in poinList at ith "row", in "column" Name
            double x = System.Convert.ToSingle(questionList[i][xName]);
            double y = System.Convert.ToSingle(questionList[i][yName]);
            double z = System.Convert.ToSingle(questionList[i][zName]);
            int result = System.Convert.ToInt32(questionList[i][outcome]);

            //Scaling to spread the plot
            x = Math.Round(x, 2);
            y = Math.Round(y, 2);
            z = Math.Round(z, 2);
            //Debug.Log("x = " + x + " \ty = " + y + "\tz = " + z + "\toutcome = " + result);

            //instantiate the prefab with coordinates defined above
            Instantiate(PointPrefab, new Vector3((float)x, (float)y, (float)z), Quaternion.identity);

            // Instantiate as gameobject variable so that it can be manipulated within loop
            GameObject dataPoint = Instantiate(
                    PointPrefab,
                    new Vector3((float)x, (float)y, (float)z),
                    Quaternion.identity);

            // Make child of PointHolder object, to keep points within container in hierarchy
            dataPoint.transform.parent = Plotter.transform;

            // Assigns original values to dataPointName
            string dataPointName =
                pointList[i][xName] + " "
                + pointList[i][yName] + " "
                + pointList[i][zName];

            // Assigns name to the prefab
            dataPoint.transform.name = dataPointName;
            
            // Gets material color and sets it to a new RGB color we define
            switch (i)
            {
                case 0:
                    dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
                    break;
                case 1:dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                    break;
                case 2:
                    dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
                    break;
                case 3:
                    dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.black);
                    break;
                case 4:
                    dataPoint.GetComponent<Renderer>().material.SetColor("_Color", Color.grey);
                    break;
            }
        }
    }
}
