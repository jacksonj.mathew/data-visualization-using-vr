﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Accord.MachineLearning.Clustering;
using Mono.Data.SqliteClient;
using System.Data;

public class TSNEData : MonoBehaviour
{
    public static void Implement(double[][] dataMatrix, int[] outcomeData, int column, int flag, int[] expectedOutcome = null)
    {
        //Debug.Log(dataMatrix.Length);
        TSNE tSNE = new TSNE()
        {
            NumberOfOutputs = 3,
            Perplexity = column
        };
        //Debug.Log("Initialisation done");

        // Reducing the data size because of the time complexity to run T-SNE
        double[][] reducedData = new double[1000][];
        for (int i=0; i<1000; i++)
        {
            reducedData[i] = dataMatrix[i];
        }
        // Transform to a reduced dimensionality space
        double[][] transformData = tSNE.Transform(reducedData);

        // Writting TSNE data to the database
        string dbName = "URI=file:" + Application.dataPath + "/DataVisualisation.db"; // The name of the db
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(dbName);
        dbconn.Open(); //Opens Connection to the database

        IDbCommand dbcmd = dbconn.CreateCommand();
        string sqlQuery;
        if (flag==0)
            sqlQuery = "DELETE FROM Banking_data_TSNE;"; //Truncate Query
        else
            sqlQuery = "DELETE FROM Election_data_TSNE;"; //Truncate Query

        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();
        for (int i = 0; i < transformData.Length; i++)
        {
            //Insert Query
            string insertQuery;
            if (flag == 0)
                insertQuery = "INSERT INTO Banking_data_TSNE ('x-value','y-value','z-value','outcome') VALUES (" + transformData[i][0] + "," + transformData[i][1] + "," + transformData[i][2] + "," + outcomeData[i] + ")";
            else
                insertQuery = "INSERT INTO Election_data_TSNE ('x-value','y-value','z-value','outcome','expected-outcome') VALUES (" + transformData[i][0] + "," + transformData[i][1] + "," + transformData[i][2] + "," + outcomeData[i] + "," + expectedOutcome[i] + ")";
            dbcmd.CommandText = insertQuery;
            reader = dbcmd.ExecuteReader();
        }
        //closing datbase connection
        reader.Close();
        reader = null;
        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
    }
}
